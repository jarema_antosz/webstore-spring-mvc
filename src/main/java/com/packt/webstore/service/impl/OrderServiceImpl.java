package com.packt.webstore.service.impl;

import com.packt.webstore.domain.Product;
import com.packt.webstore.domain.repository.ProductRepository;
import com.packt.webstore.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Jarek on 2015-09-20.
 */

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public void processOrder(String productId, long amount) {

        Product product = productRepository.getProductById(productId);

        if(product.getUnitInStock() < amount){
            throw new IllegalArgumentException("Zbyt mało sztuk towaru w magazynie. Obecna liczna sztuk:" + product.getUnitInStock());
        }

        product.setUnitInStock(product.getUnitInStock() - amount);

    }
}
