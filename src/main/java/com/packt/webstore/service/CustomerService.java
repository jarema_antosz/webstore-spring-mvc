package com.packt.webstore.service;

import com.packt.webstore.domain.Customer;

import java.util.List;

/**
 * Created by Jarek on 2015-09-20.
 */
public interface CustomerService {

    public List<Customer> getAllCustomers();
}
