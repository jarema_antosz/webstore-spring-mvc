package com.packt.webstore.service;

/**
 * Created by Jarek on 2015-09-20.
 */
public interface OrderService {

    public void processOrder(String productId, long amount);
}
