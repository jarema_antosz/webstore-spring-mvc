package com.packt.webstore.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Jarek on 2015-09-16.
 */

@Controller
public class HomeController {

    @RequestMapping("/")
    public String welcome(Model model){

        model.addAttribute("greeting", "Witaj w sklepie internetowym");
        model.addAttribute("tagline", "Jedymym i wyjątkowym sklepie");

        //redirect -> create a new request
        //forward -> forward previous request

        return "forward:/welcome/greeting";
    }

    @RequestMapping("/welcome/greeting")
    public String greeting(){
        return "welcome";
    }
}
