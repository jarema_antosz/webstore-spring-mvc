package com.packt.webstore.controller;

import com.packt.webstore.domain.Product;
import com.packt.webstore.exception.NoProductsFoundUnderCategoryException;
import com.packt.webstore.exception.ProductNotFoundException;
import com.packt.webstore.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Jarek on 2015-09-19.
 */

@Controller
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @InitBinder
    public void initializeBinder(WebDataBinder webDataBinder){
        webDataBinder.setDisallowedFields("unitInStock");
    }

    @RequestMapping
    public String list(Model model) {

        model.addAttribute("products", productService.getAllProducts());

        return "products";

    }

    @RequestMapping("/all")
    public String allProducts(Model model) {

        model.addAttribute("products", productService.getAllProducts());

        return "products";

    }

    @RequestMapping("/{category}")
    public String getProductsByCategory(Model model, @PathVariable("category") String categoryName) {

        List<Product> products = productService.getProductsByCategory(categoryName);
        if(products == null || products.isEmpty()){
            throw new NoProductsFoundUnderCategoryException();
        }
        model.addAttribute("products", products);

        return "products";

    }

    @RequestMapping("/filter/{ByCriteria}")
    public String getProductsByFilter(Model model, @MatrixVariable(pathVar = "ByCriteria") Map<String, List<String>> filterParams) {

        Set<Product> products = productService.getProductsByFilter(filterParams);



        model.addAttribute("products", products);

        return "products";

    }

    @RequestMapping("/product")
    public String getProductById(@RequestParam("id") String productId, Model model) {

        model.addAttribute("product", productService.getProductById(productId));
        return "product";

    }

    @ExceptionHandler(ProductNotFoundException.class)
    public ModelAndView handleError(HttpServletRequest request, ProductNotFoundException exception){

        ModelAndView mav = new ModelAndView();
        mav.addObject("invalidProductId", exception.getProductId());
        mav.addObject("exception", exception);
        mav.addObject("url", request.getRequestURL()+"?"+request.getQueryString());
        mav.setViewName("productNotFound");
        return mav;

    }


    //products/tablet/price;low=200;height=400?manufacturer=Google
    @RequestMapping("/{category}/{price}")
    public String getProductsByManufacture(@PathVariable String category, @MatrixVariable(pathVar = "price") Map<String,
            List<String>> priceParams,@RequestParam String manufacturer, Model model) {


        return "products";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String addNewProduct(Model model){

        model.addAttribute("newProduct", new Product());

        return "addProduct";
    }


    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String processNewProduct(@ModelAttribute("newProduct") Product newProduct, BindingResult result, HttpServletRequest request){



        String[] suppressedFields = result.getSuppressedFields();

        if(suppressedFields.length > 0){
            throw new RuntimeException("Próba użycia niedozwolonych pól:" + StringUtils.arrayToCommaDelimitedString(suppressedFields));
        }

        MultipartFile productImage = newProduct.getProductImage();
        String realPath = request.getSession().getServletContext().getRealPath("/");

        if(productImage != null && !productImage.isEmpty()){
            try {
                productImage.transferTo(new File(realPath + "resources\\images\\" + newProduct.getProductId() + ".jpg"));
            } catch (IOException e) {
                throw new RuntimeException("Niepowodzenie przy zapisie obrazka");
            }
        }
        productService.addProduct(newProduct);

        return "redirect:/products";

    }
}
