package com.packt.webstore.exception;

/**
 * Created by Jarek on 2015-10-10.
 */
public class ProductNotFoundException extends RuntimeException {

    private String productId;

    public ProductNotFoundException(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }
}
