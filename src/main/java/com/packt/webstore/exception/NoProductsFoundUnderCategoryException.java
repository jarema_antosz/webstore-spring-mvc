package com.packt.webstore.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Jarek on 2015-10-10.
 */

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Brak produktów we wskazanej kategorii")
public class NoProductsFoundUnderCategoryException extends RuntimeException {
}
