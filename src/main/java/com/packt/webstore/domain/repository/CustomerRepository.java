package com.packt.webstore.domain.repository;

import com.packt.webstore.domain.Customer;

import java.util.List;

/**
 * Created by Jarek on 2015-09-20.
 */
public interface CustomerRepository {

    public List<Customer> getAllCustomers();
}
