package com.packt.webstore.domain.repository.impl;

import com.packt.webstore.domain.Product;
import com.packt.webstore.domain.repository.ProductRepository;
import com.packt.webstore.exception.ProductNotFoundException;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * Created by Jarek on 2015-09-19.
 */

@Repository
public class InMemoryProductRepository implements ProductRepository {

    private List<Product> allProducts = new ArrayList<>();


    public InMemoryProductRepository() {

        Product iphone = new Product();
        iphone.setProductId("P1234");
        iphone.setName("iPhone5");
        iphone.setUnitInStock(1000);
        iphone.setCategory("Smartfon");
        iphone.setManufacturer("apple");

        allProducts.add(iphone);

        Product laptopDell = new Product();
        laptopDell.setProductId("P1235");
        laptopDell.setName("Laptop Dell");
        laptopDell.setUnitInStock(1000);
        laptopDell.setCategory("Laptop");
        laptopDell.setManufacturer("dell");

        allProducts.add(laptopDell);

        Product tabletNexus = new Product();
        tabletNexus.setProductId("P1236");
        tabletNexus.setName("Tablet Nexus");
        tabletNexus.setUnitInStock(1000);
        tabletNexus.setCategory("Tablet");
        tabletNexus.setManufacturer("google");

        allProducts.add(tabletNexus);
    }

    @Override
    public Product getProductById(String productId) {

        Product productById = null;

        for (Product product : allProducts) {
            if (product.getProductId().equals(productId)) {
                productById = product;
                break;
            }
        }

        if (productById == null) {
            throw new ProductNotFoundException(productId);
        }

        return productById;
    }

    @Override
    public List<Product> getAllProducts() {
        return allProducts;
    }

    @Override
    public List<Product> getProductsByCategory(String category) {
        List<Product> products = new ArrayList<>();

        for (Product product : allProducts) {
            if (product.getCategory().equals(category)) {
                products.add(product);
            }
        }

        return products;
    }

    @Override
    public Set<Product> getProductsByFilter(Map<String, List<String>> filterParams) {

        Set<Product> productsByBrand = new HashSet<>();
        Set<Product> productsByCategory = new HashSet<>();
        Set<String> criterias = filterParams.keySet();

        if (criterias.contains("brand")) {
            for (String brand : filterParams.get("brand")) {
                for (Product product : allProducts) {
                    if (product.getManufacturer().equalsIgnoreCase(brand)) {
                        productsByBrand.add(product);
                    }
                }
            }
        }

        if (criterias.contains("category")) {
            for (String category : filterParams.get("category")) {
                for (Product product : allProducts) {
                    if (product.getCategory().equalsIgnoreCase(category)) {
                        productsByCategory.add(product);
                    }
                }
            }
        }


        productsByCategory.retainAll(productsByBrand);

        return productsByCategory;
    }

    @Override
    public void addProduct(Product product) {
        allProducts.add(product);
    }
}
