package com.packt.webstore.domain.repository.impl;

import com.packt.webstore.domain.Customer;
import com.packt.webstore.domain.repository.CustomerRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jarek on 2015-09-20.
 */

@Repository
public class InMemoryCustomerRepository implements CustomerRepository {

    List<Customer> allCustomers = new ArrayList<>();

    public InMemoryCustomerRepository(){

        Customer customerJarek = new Customer("C1234", "Jarek", "Testowa Ulica 12/1, 20-100 Testowe Miasto", 5);
        allCustomers.add(customerJarek);

        Customer customerMichal = new Customer("C1235", "Michał", "Testowa Ulica 23/2, 20-100 Testowe Miasto", 5);
        allCustomers.add(customerMichal);

    }

    @Override
    public List<Customer> getAllCustomers() {
        return allCustomers;
    }
}
