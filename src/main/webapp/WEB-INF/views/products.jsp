<%--
  Created by IntelliJ IDEA.
  User: Jarek
  Date: 2015-09-19
  Time: 20:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title>produkty</title>
</head>
<body>

<c:forEach items="${products}" var="product">
    <h1>${product.productId}</h1>

    <h1>${product.name}</h1>
    <h1>${product.unitInStock}</h1>
    <h1>${product.condition}</h1>
    <h1><a href="<spring:url value="/products/product?id=${product.productId}"/>">szczegóły</a></h1>

</c:forEach>
</body>
</html>
