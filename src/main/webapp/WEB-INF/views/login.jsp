<%--
  Created by IntelliJ IDEA.
  User: Jarek
  Date: 2015-10-03
  Time: 17:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title></title>
</head>
<body>

<div>
    <c:if test="${not empty error}"><h2><spring:message code="AbstractUserDetailsAuthenticationProvider.badCredentials"/></h2></c:if>

    <form action="<c:url value="/j_spring_security_check"/>" method="post">
        <fieldset>
            <input type="text" name="j_username"/>
            <input type="password" name="j_password"/>
            <input type="submit" value="Zaloguj się">
        </fieldset>
    </form>


</div>

</body>
</html>
