<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head></head>
<body>
    <form:form modelAttribute="newProduct" enctype="multipart/form-data">
        <fieldset>
            <legend></legend>
            <div><label><spring:message code="addProduct.form.productId.label"/></label>
            <form:input path="productId" id="productId" type="text"/>
            </div>
            <div><label><spring:message code="addProduct.form.productName.label"/></label>
                <form:input path="name" id="name" type="text"/>
            </div>
            <%--<div><label>unitInStock</label>--%>
                <%--<form:input path="unitInStock" id="unitInStock" type="text"/>--%>
            <%--</div>--%>
            <div><label>stan</label>
                <form:radiobutton path="condition" value="nowy"/>nowy
                <form:radiobutton path="condition" value="odnowiony"/>odnowiony
            </div>
            <div>
                <spring:message code="addProduct.form.productImage.label"/>
                <form:input path="productImage" type="file" id="productImage"/>
            </div>

            <div><input type="submit" value="Dodaj"></div>

        </fieldset>
    </form:form>
    <h3><a href="/j_spring_security_logout">Wyloguj się</a> </h3>
</body>
</html>